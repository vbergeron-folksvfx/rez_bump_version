# -*- coding: utf-8 -*-
"""
Authors: Vincent Bergeron
"""


name = "rez_bump_version"

version = "1.0.1"

description = "Bump Version of package.py"

requires = ['python']


def commands():
    # App Module
    env.PYTHONPATH.append('{root}/bin')
    # Alias
    alias('{this.name}', 'python {root}/bin/{this.name}.py')


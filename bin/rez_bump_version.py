# -*- coding: utf-8 -*-
"""

Author: Vincent Bergeron

"""
import os
import re
import argparse


class VersionBumper(object):
    MAJOR_VERSION_REGEX = re.compile('(?<=^)([0-9]+)(?=\.)')
    MINOR_VERSION_REGEX = re.compile('(?<=\.)([0-9]+)(?=\.)')
    BUGFIX_VERSION_REGEX = re.compile('(?<=\.)([0-9]+)$')

    def __init__(self, package_py, update_major=False, update_minor=False, update_bugfix=False, dry=False):
        self.package_py = package_py
        self.dry = dry
        self.update_major = update_major
        self.update_minor = update_minor
        self.update_bugfix = update_bugfix

        self.major_version = int
        self.minor_version = int
        self.bugfix_version = int

        self.current_version = self._find_current_version()
        self._retrieve_versions_by_type()

    def execute(self):
        update = None

        if self.update_major:
            update = 'major'
            self.major_version = int(self.major_version) + 1
            self.minor_version = 0
            self.bugfix_version = 0

        elif self.update_minor:
            update = 'minor'
            self.minor_version = int(self.minor_version) + 1
            self.bugfix_version = 0

        elif self.update_bugfix:
            update = 'bugfix'
            self.bugfix_version = int(self.bugfix_version) + 1

        if not update:
            raise Exception('No version type provided. (major, minor, bugfix)')

        updated_version = '{0}.{1}.{2}'.format(self.major_version,
                                               self.minor_version,
                                               self.bugfix_version)

        print('{0} update: {1} --> {2}'.format(update.capitalize(),
                                               self.current_version,
                                               updated_version))
        if self.dry:
            print('DRY.')
            return

        self._bump_version(updated_version)

    def _retrieve_versions_by_type(self):
        self.major_version = re.search(self.MAJOR_VERSION_REGEX, self.current_version).group()
        self.minor_version = re.search(self.MINOR_VERSION_REGEX, self.current_version).group()
        self.bugfix_version = re.search(self.BUGFIX_VERSION_REGEX, self.current_version).group()

    def _bump_version(self, new_version):
        with open(self.package_py, 'r') as package_py:
            loaded_package_py = package_py.read()

            new_content = loaded_package_py.replace(self.current_version,
                                                    new_version)
        package_py.close()

        with open(self.package_py, 'w') as new_package_py:
            new_package_py.write(new_content)

        new_package_py.close()

    def _find_current_version(self):
        with open(self.package_py, 'r') as package_py:
            loaded_package_py = package_py.read()

        found_version = re.search('(?<=version).+(?<=\"|\')([0-9.]+)(?=\"|\')',
                                  loaded_package_py)
        package_py.close()
        return found_version.group(1)

    @classmethod
    def parse_args(cls):
        parser = argparse.ArgumentParser()
        parser.add_argument("-p", "--package", help='Package.py to version up')
        parser.add_argument("-M", "--major", action='store_true', help="Bump Major Version")
        parser.add_argument("-m", "--minor", action='store_true', help="Bump Minor Version")
        parser.add_argument("-b", "--bugfix", action='store_true', help="Bump Bug Fix Version")
        parser.add_argument("-rd", "--recursive_dir", help="Provide a directory to recursively update all package.py")
        parser.add_argument("-d", "--dry", action='store_true', help="Dry run to simply print out actions.")
        args = parser.parse_args()

        recursive = args.recursive_dir
        package_py = args.package
        if not recursive:
            if not package_py:
                raise Exception('No package.py was provided. Please provide one as first argument.')
        else:
            if recursive.startswith(('.', '_')):
                raise Exception('Please provide a valid path.')
            if not recursive.startswith('/home'):
                raise Exception('Recursive can not be used outside of your home dir')
            print('Recursive mode will be used. Provided package.py ignored.')

        if recursive:
            for root, dirs, files in os.walk(recursive):
                for a_file in sorted(files):
                    # some condition
                    if 'package.py' == a_file:
                        found_package_py = os.path.join(root, a_file)
                        print('Attempting to update: {}'.format(found_package_py))
                        bump_it = cls(found_package_py, args.major, args.minor, args.bugfix, args.dry)
                        bump_it.execute()
            return False

        return cls(args.package, args.major, args.minor, args.bugfix, args.dry)


if __name__ == '__main__':
    bump = VersionBumper.parse_args()
    if bump:
        bump.execute()

This CLI tool's purpose to give Pipeline TDs the possibility to easily upgrade Rez Package Versions, whether it's major, minor or bugfix versions.

--help for more information

--help as of version 1.0.0:

```
usage: rez_bump_version.py [-h] [-p PACKAGE] [-M] [-m] [-b]
                           [-rd RECURSIVE_DIR] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -p PACKAGE, --package PACKAGE
                        Package.py to version up
  -M, --major           Bump Major Version
  -m, --minor           Bump Minor Version
  -b, --bugfix          Bump Bug Fix Version
  -rd RECURSIVE_DIR, --recursive_dir RECURSIVE_DIR
                        Provide a directory to recursively update all
                        package.py
  -d, --dry             Dry run to simply print out actions
```

